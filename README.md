
# OFF colorz

## Description

`:syntax on`’s OFF mode. I like to code with syntax off, but I want stuff like
comments, todos and errors to stand out. So I created these color schemes.

## Schemes

### Nuptiale

#### Normal mode

![Screenshot of a Javascript document in normal mode](https://bitbucket.org/iamkvein/off-colorz/raw/7a2dabbd06a341c25c448867faad0b482de78267/screenshots/nuptiale_javascript_normal.png)

#### Insert mode

![Screenshot of an HTML document in insert mode](https://bitbucket.org/iamkvein/off-colorz/raw/7a2dabbd06a341c25c448867faad0b482de78267/screenshots/nuptiale_html_insert.png)

