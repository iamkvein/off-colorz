" Nuptiale Vim color file
" Maintainer: Kevin Faber <kevin.fab@gmail.com>
" Last Change: 15/10/2013
" Description: First OFF colorz scheme ever

" ------------------------------------------------------------------------------
" Setup

set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif

let g:colors_name="nuptiale"

" ------------------------------------------------------------------------------
" Colors

let s:gui_dark = [
  \ '#252226',
  \ '#2c292d',
  \ '#1e1d1f',
  \ '#270c17'
\ ]
let s:gui_light = [
  \ '#8f888b',
  \ '#4a454b',
  \ '#ffffff',
  \ '#7a6082',
  \ '#6c5873',
  \ '#b0a4a8',
  \ '#71676c'
\ ]
let s:gui_color = [
  \ '#d41660',
  \ '#9e154a',
  \ '#c551ed',
  \ '#552e62',
  \ '#6d5784',
  \ '#4c172c',
  \ '#916bb7'
\ ]

let s:cterm_dark = [
  \ '233',
  \ '233',
  \ '233',
  \ '52'
\ ]
let s:cterm_light = [
  \ '247',
  \ '234',
  \ '254',
  \ '242',
  \ '240',
  \ '2428',
  \ '242'
\ ]
let s:cterm_color = [
  \ '198',
  \ '198',
  \ '199',
  \ '128',
  \ '128',
  \ '89',
  \ '129',
  \ '45',
  \ '221',
  \ '203',
  \ '209'
\ ]

" ------------------------------------------------------------------------------
" Highlights

exec "hi Normal guibg=". s:gui_dark[0]  ." guifg=". s:gui_light[0] ." ctermbg=". s:cterm_dark[0]  ." ctermfg=". s:cterm_light[0]
exec "hi Statement guibg=bg guifg=fg gui=none ctermbg=bg ctermfg=fg"
exec "hi Identifier guibg=bg guifg=fg gui=none ctermbg=bg ctermfg=fg"
exec "hi Type guibg=bg guifg=fg gui=none ctermbg=bg ctermfg=fg"
exec "hi Constant guibg=bg guifg=fg gui=none ctermbg=bg ctermfg=fg"
exec "hi PreProc guibg=bg guifg=fg gui=none ctermbg=bg ctermfg=fg"
exec "hi Special guibg=bg guifg=fg gui=none ctermbg=bg ctermfg=fg"
exec "hi Underlined guibg=bg guifg=fg gui=none ctermbg=bg ctermfg=fg"
exec "hi Cursor guibg=". s:gui_color[0] ." guifg=bg ctermbg=". s:cterm_color[0] ." ctermfg=bg"
exec "hi CursorLine guibg=". s:gui_dark[1] ." guifg=fg"

exec "hi LineNr guibg=". s:gui_dark[2] ." guifg=". s:gui_color[1] ." ctermbg=". s:cterm_dark[2] ." ctermfg=". s:cterm_color[1]
exec "hi CursorLineNr guibg=". s:gui_dark[2] ." guifg=". s:gui_color[0] ." ctermbg=". s:cterm_dark[2] ." ctermfg=". s:cterm_color[0]
exec "hi SignColumn guibg=". s:gui_dark[2] ." guifg=". s:gui_color[1] ." ctermbg=". s:cterm_dark[2] ." ctermfg=". s:cterm_color[1]

exec "hi NonText guifg=". s:gui_color[1] ." ctermfg=". s:cterm_color[1]
exec "hi Search guibg=". s:gui_color[3] ." guifg=". s:gui_color[2] ." ctermbg=". s:cterm_color[3] ." ctermfg=". s:cterm_color[2]
exec "hi IncSearch guibg=". s:gui_color[2] ." guifg=". s:gui_color[3] ." ctermbg=". s:cterm_color[2] ." ctermfg=". s:cterm_color[3]

exec "hi PMenu guibg=". s:gui_dark[2] ." guifg=". s:gui_color[4] ." ctermbg=". s:cterm_dark[2] ." ctermfg=". s:cterm_color[4]
exec "hi PMenuSel guibg=". s:gui_color[2] ." guifg=". s:gui_color[3] ." ctermbg=". s:cterm_color[2] ." ctermfg=". s:cterm_color[3]
exec "hi PMenuSbar guibg=". s:gui_dark[2] ." ctermbg=". s:cterm_dark[2]
exec "hi PMenuThumb guibg=". s:gui_color[4] ." ctermbg=". s:cterm_color[4]

exec "hi Visual guibg=". s:gui_color[5] ." guifg=". s:gui_color[0] ." ctermbg=". s:cterm_color[5] ." ctermfg=". s:cterm_color[0]

exec "hi VertSplit guibg=". s:gui_color[1] ." guifg=bg ctermbg=". s:cterm_color[1] ." ctermfg=bg"

exec "hi SpellBad gui=undercurl guisp=". s:gui_color[0] ." ctermfg=". s:cterm_color[0]
exec "hi SpellCap gui=undercurl guisp=". s:gui_color[6] ." ctermfg=". s:cterm_color[6]

exec "hi Directory guibg=bg guifg=". s:gui_light[3] ." ctermbg=bg ctermfg=". s:cterm_light[3]
exec "hi SpecialKey guibg=bg guifg=". s:gui_light[3] ." ctermbg=bg ctermfg=". s:cterm_light[3]

exec "hi ErrorMsg guibg=". s:gui_light[2] ." guifg=". s:gui_color[0] ." ctermbg=". s:cterm_light[2] ." ctermfg=". s:cterm_color[0]
exec "hi Error guibg=". s:gui_light[2] ." guifg=". s:gui_color[0] ." ctermbg=". s:cterm_light[2] ." ctermfg=". s:cterm_color[0]
exec "hi WarningMsg guibg=". s:gui_color[5] ." guifg=". s:gui_light[2] ." ctermbg=". s:cterm_color[5] ." ctermfg=". s:cterm_light[2]
exec "hi MoreMsg guibg=bg guifg=fg gui=bold ctermbg=bg ctermfg=fg"

exec "hi WildMenu guibg=". s:gui_color[0] ." guifg=". s:gui_dark[3] ." gui=bold"

exec "hi MatchParen guibg=bg guifg=". s:gui_color[0] ." ctermbg=bg ctermfg=". s:cterm_color[0]
exec "hi Question guibg=bg guifg=". s:gui_color[0] ." gui=bold" ." ctermbg=bg ctermfg=". s:cterm_color[0]

exec "hi Folded guibg=". s:gui_dark[3] ." guifg=". s:gui_color[1] ." ctermbg=". s:cterm_dark[2] ." ctermfg=". s:cterm_color[1]
exec "hi FoldColumn guibg=". s:gui_dark[3] ." guifg=". s:gui_color[1] ." ctermbg=". s:cterm_dark[2] ." ctermfg=". s:cterm_color[1]

exec "hi Comment guibg=bg guifg=". s:gui_light[4] ." ctermbg=bg ctermfg=". s:cterm_light[4]
exec "hi Todo guibg=bg guifg=". s:gui_light[5] ." gui=bold ctermbg=bg ctermfg=". s:cterm_light[5]
exec "hi Title guibg=bg guifg=". s:gui_color[0] ." gui=bold ctermbg=bg ctermfg=". s:cterm_color[0]

exec "hi EasyMotionTarget guibg=bg guifg=". s:gui_color[0] ." gui=bold ctermbg=bg ctermfg=". s:cterm_color[0]
exec "hi EasyMotionTargetDefault guibg=bg guifg=". s:gui_color[0] ." ctermbg=bg ctermfg=". s:cterm_color[0]

exec "hi TabLine ctermbg=". s:cterm_dark[2] ." ctermfg=". s:cterm_color[1]
exec "hi TabLineSel ctermbg=". s:cterm_color[1] ." ctermfg=". s:cterm_dark[2]
exec "hi TabLineFill ctermbg=". s:cterm_dark[1] ." ctermfg=". s:cterm_light[1]

" TODO gui diff
exec "hi DiffAdd ctermbg=". s:cterm_color[7] ." ctermfg=". s:cterm_dark[0]
exec "hi DiffChange ctermbg=". s:cterm_color[8] ." ctermfg=". s:cterm_dark[0]
exec "hi DiffDelete ctermbg=". s:cterm_color[9] ." ctermfg=". s:cterm_dark[0]
exec "hi DiffText ctermbg=". s:cterm_color[10] ." ctermfg=". s:cterm_dark[0]

" ------------------------------------------------------------------------------
" Linking

hi link Typedef Normal
hi link Structure Normal
hi link StorageClass Normal
hi link Conditional Normal
hi link Repeat Normal
hi link Operator Normal
hi link Keyword Normal
hi link Include Normal
hi link Define Normal
hi link Macro Normal
hi link PreCondit Normal
hi link Tag Normal
hi link SpecialChar Normal
hi link Delimiter Normal
hi link Debug Normal
hi link Exception Normal
hi link Label Normal
hi link Repeat Normal
hi link Function Normal
hi link Float Normal
hi link Boolean Normal
hi link Number Normal
hi link Character Normal
hi link String Normal
hi link multiple_cursors_cursor Cursor

" ------------------------------------------------------------------------------
" Status line

function! s:StatuslineInsertColor(mode)
  if a:mode == 'i'
    exec "hi StatusLine guibg=". s:gui_color[0] ." guifg=". s:gui_dark[3] ." gui=none"
  elseif a:mode == 'r'
    exec "hi StatusLine guibg=". s:gui_color[0] ." guifg=". s:gui_dark[3] ." gui=none"
  else
    exec "hi StatusLine guibg=". s:gui_dark[3] ." guifg=". s:gui_color[0] ." gui=none"
  endif
endfunction

augroup nuptialecolorz_statusline_insert
  " Change color on mode switch
  au InsertEnter * call s:StatuslineInsertColor(v:insertmode)
  exec "au InsertLeave *hi StatusLine guibg=". s:gui_dark[3] ." guifg=". s:gui_color[0] ." gui=bold"
augroup END

exec "hi StatusLine guibg=". s:gui_dark[3] ." guifg=". s:gui_color[0] ." gui=bold ctermbg=". s:cterm_color[1] ." ctermfg=". s:cterm_light[2]
exec "hi StatusLineNC guibg=". s:gui_dark[2] ." guifg=". s:gui_light[6] ." gui=bold ctermbg=". s:cterm_light[6] ." ctermfg=". s:cterm_dark[2]

