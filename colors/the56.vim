" The56 Vim color file
" Maintainer: Kevin Faber <kevin.fab@gmail.com>

" ------------------------------------------------------------------------------
" Setup

set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif

let g:colors_name="the56"

hi Normal cterm=none ctermfg=252 ctermbg=232
hi Identifier cterm=none ctermfg=252 ctermbg=232
hi Keyword cterm=none ctermfg=252 ctermbg=232
hi Function cterm=none ctermfg=252 ctermbg=232
hi Type cterm=none ctermfg=252 ctermbg=232
hi Constant cterm=none ctermfg=252 ctermbg=232
hi Preproc cterm=none ctermfg=252 ctermbg=232
hi Underlined cterm=none ctermfg=252 ctermbg=232
hi NonText cterm=none ctermfg=252 ctermbg=232
hi Question cterm=none ctermfg=252 ctermbg=232
hi MoreMsg cterm=none ctermfg=252 ctermbg=232
hi ModeMsg cterm=none ctermfg=252 ctermbg=232
hi SpecialKey cterm=none ctermfg=252 ctermbg=232
hi Directory cterm=none ctermfg=252 ctermbg=232
hi htmlItalic cterm=none ctermfg=252 ctermbg=232
hi htmlBold cterm=none ctermfg=252 ctermbg=232
hi htmlUnderline cterm=none ctermfg=252 ctermbg=232
hi htmlBoldItalic cterm=none ctermfg=252 ctermbg=232
hi htmlBoldUnderline cterm=none ctermfg=252 ctermbg=232
hi htmlBoldUnderlineItalic cterm=none ctermfg=252 ctermbg=232
hi htmlUnderlineItalic cterm=none ctermfg=252 ctermbg=232

hi Statement cterm=none ctermfg=246 ctermbg=232
hi Operator cterm=bold ctermfg=246 ctermbg=232
hi javascriptFuncKeyword cterm=none ctermfg=246 ctermbg=232
hi javascriptVariable cterm=none ctermfg=246 ctermbg=232

hi Special cterm=none ctermfg=198 ctermbg=232

hi Title cterm=none ctermfg=51 ctermbg=232

hi Comment cterm=none ctermfg=51 ctermbg=232
hi Todo cterm=none ctermfg=235 ctermbg=51

hi Visual cterm=none ctermfg=235 ctermbg=50
hi WildMenu cterm=none ctermfg=235 ctermbg=50

hi Search cterm=none ctermfg=235 ctermbg=226
hi IncSearch cterm=none ctermfg=235 ctermbg=226

hi StatusLine cterm=none ctermfg=244 ctermbg=235
hi StatusLineNC cterm=none ctermfg=244 ctermbg=235
hi VertSplit cterm=none ctermfg=244 ctermbg=235

hi Conceal cterm=none ctermfg=244 ctermbg=235

hi Error cterm=none ctermfg=015 ctermbg=198
hi ErrorMsg cterm=none ctermfg=015 ctermbg=198

hi LineNr cterm=none ctermfg=198 ctermbg=232
hi CursorLineNr cterm=none ctermfg=51 ctermbg=232
hi SignColumn cterm=none ctermfg=198 ctermbg=232

hi Folded cterm=bold ctermfg=198 ctermbg=232
hi FoldColumn cterm=bold ctermfg=198 ctermbg=232

hi CursorLine cterm=bold ctermfg=015 ctermbg=232
hi CursorColumn cterm=bold ctermfg=015 ctermbg=232
hi ColorColumn cterm=bold ctermfg=015 ctermbg=232

hi PMenu ctermfg=232 ctermbg=198
hi PMenuSel ctermfg=198 ctermbg=015
hi PMenuSbar ctermfg=232 ctermbg=015
hi PMenuThumb ctermfg=232 ctermbg=015

hi TabLine cterm=none ctermfg=252 ctermbg=235
hi TabLineSel cterm=none ctermfg=235 ctermbg=198
hi TabLineFill cterm=none ctermfg=252 ctermbg=235

hi DiffAdd cterm=none ctermfg=232 ctermbg=048
hi DiffChange cterm=none ctermfg=232 ctermbg=214
hi DiffDelete cterm=none ctermfg=232 ctermbg=197
hi DiffText cterm=none ctermfg=214 ctermbg=232

hi SpellCap cterm=none ctermfg=232 ctermbg=048
hi SpellRare cterm=none ctermfg=232 ctermbg=214
hi SpellBad cterm=none ctermfg=232 ctermbg=197
hi SpellLocal cterm=none ctermfg=214 ctermbg=232

hi MatchParen ctermfg=235 ctermbg=51
hi Cursor ctermfg=235 ctermbg=198
